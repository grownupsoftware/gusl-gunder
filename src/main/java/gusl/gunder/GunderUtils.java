package gusl.gunder;

import java.io.File;

/**
 * Shared Utils.
 *
 * @author dhudson
 */
public class GunderUtils {

    private GunderUtils() {
    }

    public static File getTmpDirectory() {
        return new File(getTmpDirectoryPath());
    }

    public static String getTmpDirectoryPath() {
        return System.getProperty("java.io.tmpdir");
    }
}
