package gusl.gunder;

import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.initialization.dsl.ScriptHandler;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Gradle Gunder Plugin.
 * <p>
 * This link is a good example of what you can do in a plugin, it has no
 * relevance to this plugin. I just use it as a cook book.
 * <p>
 * http://grepcode.com/file/repo1.maven.org/maven2/io.github.celestibytes/CelestiGradle/2.4.2/celestibytes/gradle/CelestiGradlePlugin.java
 *
 * @author dhudson
 */
public class GunderPlugin implements Plugin<Project> {

    private final Logger theLogger = Logging.getLogger(GunderPlugin.class);
    private final String LAUNCHER = "gusl-launcher";

    /**
     * Entry point for the Gradle Plugin.
     *
     * @param target
     */
    @Override
    public void apply(Project target) {

        //theLogger.quiet(" ---> +{}", getClass().getProtectionDomain().getCodeSource().getLocation().getFile());
        // After everything has been evaluated, do this
        target.afterEvaluate(new Action<Project>() {
            @Override
            public void execute(Project project) {
                // Find the Casanova-Launcher jar, which is a fat jar and has everything we need in it.
                File launcher = null;

                // Lets try and add dependencies
                for (GunderRunWarTask gunderTask : getGunderWarTasks(project)) {

                    // Lets find this
                    if (launcher == null) {
                        launcher = findRequiredJar(target);
                        theLogger.info("I have a {} of {} for project {}", LAUNCHER, launcher, project.getPath());
                    }

                    // Create a debug task
                    String debugTaskName = gunderTask.getName() + "Debug";
                    GunderRunWarTask debugTask = project.getTasks().create(debugTaskName, GunderRunWarTask.class);
                    debugTask.setDebugEnabled(true);

                    StringBuilder builder = new StringBuilder(100);

                    if (gunderTask.getDescription() != null) {
                        builder.append(gunderTask.getDescription());
                        builder.append(" ");
                    }

                    builder.append("(Debug : port ");
                    builder.append(gunderTask.debugPort);
                    builder.append(", Suspend ");
                    builder.append(gunderTask.suspend);
                    builder.append(" )");

                    debugTask.setDescription(builder.toString());

                    // If the project is a web app project then add the war dependency
                    if (GradleUtils.hasPlugin(project, "war")) {
                        if (!gunderTask.isExcludeSelf()) {
                            // Add thy self
                            gunderTask.webapp(project.getPath());
                            debugTask.webapp(project.getPath());

                            theLogger.info(" ......................   Adding ...... ", project.getPath());
                        }
                    }

                    // Make sure that all of the web apps are a dependancy
                    for (String webApp : gunderTask.getWebapps()) {
                        if (webApp.startsWith(":")) {
                            gunderTask.dependsOn(webApp + ":war");
                            debugTask.webapp(webApp);
                            debugTask.dependsOn(webApp + ":war");
                        } else {
                            gunderTask.webapp(webApp);
                        }
                    }

                    // Copy over the args from the normal task ..
                    gunderTask.setRequiredJar(launcher);
                    debugTask.setRequiredJar(launcher);
                    debugTask.setHost(gunderTask.getHost());
                    debugTask.setPort(gunderTask.getPort());
                    debugTask.setDebugPort(gunderTask.getDebugPort());
                    debugTask.setDebugSuspend(gunderTask.isDebugSuspend());
                    debugTask.setLogLocation(gunderTask.getLogLocation());
                    debugTask.setRepoLocation(gunderTask.getRepoLocation());
                    debugTask.setMode(gunderTask.getMode());
                    debugTask.setInstance(gunderTask.getInstance());
                    debugTask.setJvmArgs(gunderTask.getJvmArgs());
                    debugTask.setSystemEnv(gunderTask.getSystemEnv());
                }
            }
        });
    }

    /**
     * Currently not used, but you could have something like
     * <code>grotty { ... }</code> defined in the .gradle files.
     *
     * @param project
     */
    private void addExtensions(Project project) {
        project.getExtensions().add("gunder", new GunderExtension());
    }

    private List<GunderRunWarTask> getGunderWarTasks(Project project) {
        List<GunderRunWarTask> gunderTasks = new ArrayList<>(2);

        Iterator<Task> tasks = project.getTasks().iterator();
        while (tasks.hasNext()) {
            Task task = tasks.next();
            if (task instanceof GunderRunWarTask) {
                gunderTasks.add((GunderRunWarTask) task);
            }
        }

        return gunderTasks;
    }

    /**
     * Get the dependencies of the plugin.
     * <p>
     * We are looking the casanova-launcher jar.
     *
     * @param project
     */
    private File findRequiredJar(Project project) throws GunderException {

        Configuration configuration = project.getBuildscript().getConfigurations().getByName(ScriptHandler.CLASSPATH_CONFIGURATION);

        for (File file : configuration.resolve()) {
            if (file.exists()) {
                theLogger.info("Build Script {}:{}", file.getName(), file.getName().startsWith(LAUNCHER));
                if (file.getName().startsWith(LAUNCHER)) {
                    return file;
                }
            }
        }

        // If the plugin has been applied from the parent, lets see if we can find it
        if (project.getParent() == null) {
            throw new GunderException("Can't find " + LAUNCHER + " on buildscript path");
        } else {
            return findRequiredJar(project.getParent());
        }
    }
}
