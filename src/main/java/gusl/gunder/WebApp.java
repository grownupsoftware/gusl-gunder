package gusl.gunder;

/**
 * Web App Pojo.
 *
 * @author dhudson
 */
public class WebApp {

    // I'm on the ...
    private String warPath;
    private String contextPath;

    public WebApp() {
    }

    public String getWarPath() {
        return warPath;
    }

    public void setWarPath(String warPath) {
        this.warPath = warPath;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }        
}
