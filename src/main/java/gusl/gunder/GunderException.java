package gusl.gunder;

import org.gradle.api.GradleException;

/**
 * Grunder Exception.
 * 
 * @author dhudson
 */
public class GunderException extends GradleException {

    private static final long serialVersionUID = 1L;

    public GunderException(String reason) {
        super(reason);
    }

    public GunderException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
