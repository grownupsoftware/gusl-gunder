package gusl.gunder;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Gradle Task.
 * <p>
 * Use project info to launch an Application instance.
 * <p>
 * This class holds all settings for the task.
 *
 * @author dhudson
 */
public class GunderTask extends DefaultTask {

    /**
     * Port for Undertow to listen on
     */
    public int port = 8080;

    public String host;

    /**
     * specify the debug port, defaults to 5006
     */
    public int debugPort = 5006;

    /**
     * switch on and off the debug, default to debugEnabled
     */
    public boolean debugEnabled = false;

    /**
     * Wait for the debugger to attach
     */
    public boolean suspend = false;

    /**
     * The Location of the logs.
     */
    public String logLocation;

    /**
     * The location of the message bus repository.
     */
    public String repoLocation;

    public Long entitySize = 4096000L;

    /**
     * The Operational Mode. CasanovaMode
     */
    public String mode = "DEV";

    /**
     * UK, IE, COM
     */
    public String instance = "UK";

    /**
     * JVM Arguments
     */
    private String[] jvmArgs = new String[0];

    private String[] systemEnv = new String[0];

    /**
     * Set of web apps
     */
    private final Set<String> webapps = new LinkedHashSet<>();

    /**
     * Plugin dependencies
     */
    private File theRequiredJar;

    /**
     * Don't add yourself (calling war project)
     */
    private boolean excludeSelf = false;

    /**
     * Add trace info.
     */
    public boolean trace = false;

    public boolean launcherDebug = false;

    @Input
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Input
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Input
    public int getDebugPort() {
        return debugPort;
    }

    public void setDebugPort(int debugPort) {
        this.debugPort = debugPort;
    }

    @Input
    public boolean isDebugSuspend() {
        return suspend;
    }

    public void setDebugSuspend(boolean suspend) {
        this.suspend = suspend;
    }

    @Input
    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public void setDebugEnabled(boolean debugEnabled) {
        this.debugEnabled = debugEnabled;
    }

    @Input
    public boolean isLauncherDebug() {
        return launcherDebug;
    }

    public void setLauncherDebug(boolean debug) {
        launcherDebug = debug;
    }

    @InputDirectory
    @Optional
    public String getLogLocation() {
        return logLocation;
    }

    public void setLogLocation(String logLocation) {
        this.logLocation = logLocation;
    }

    @InputDirectory
    @Optional
    public String getRepoLocation() {
        return repoLocation;
    }

    public void setRepoLocation(String repoLocation) {
        this.repoLocation = repoLocation;
    }

    @Input
    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @Input
    public String[] getJvmArgs() {
        return jvmArgs;
    }

    public void setJvmArgs(String[] jvmArgs) {
        this.jvmArgs = jvmArgs;
    }

    @Input
    public String[] getSystemEnv() {
        return systemEnv;
    }

    public void setSystemEnv(String[] envs) {
        this.systemEnv = envs;
    }

    public void webapp(String projectName) {
        webapps.add(projectName);
    }

    @Input
    public Set<String> getWebapps() {
        return webapps;
    }

    @Input
    @Internal
    public File getRequiredJar() {
        return theRequiredJar;
    }

    public void setRequiredJar(File requiredJar) {
        theRequiredJar = requiredJar;
    }

    @Input
    public boolean isExcludeSelf() {
        return excludeSelf;
    }

    public void setExcludeSelf(boolean excludeSelf) {
        this.excludeSelf = excludeSelf;
    }

    @Input
    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    @Input
    public Long getEntitySize() {
        return entitySize;
    }

    public void setEntitySize(Long entitySize) {
        this.entitySize = entitySize;
    }

    @Override
    @Internal
    public String getGroup() {
        return "Gunder";
    }

}
