package gusl.gunder;

/**
 * This defines the Grunder {} in the build.gradle
 *
 * Currently not used.
 *
 * @author dhudson
 */
public class GunderExtension {

    private int port;
    private String host;
    private int debugPort;
    private Boolean debugSuspend;
    private String[] jvmArgs;

    public GunderExtension() {
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getDebugPort() {
        return debugPort;
    }

    public void setDebugPort(int debugPort) {
        this.debugPort = debugPort;
    }

    public boolean isDebugSuspend() {
        return debugSuspend;
    }

    public void setDebugSuspend(boolean debugSuspend) {
        this.debugSuspend = debugSuspend;
    }

    public String[] getJvmArgs() {
        return jvmArgs;
    }

    public void setJvmArgs(String[] jvmArgs) {
        this.jvmArgs = jvmArgs;
    }

}
