package gusl.gunder;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Task that runs the Grotty task.
 *
 * @author dhudson
 */
public class GunderRunWarTask extends GunderTask {

    private static final Logger theLogger = Logging.getLogger(GunderRunWarTask.class);

    public GunderRunWarTask() {
    }

    @TaskAction
    public void runWarDebugTask() {

        String buildPath = getProject().getBuildDir().getAbsolutePath();

        try {

            List<String> wars = new ArrayList<>();

            for (String webAppName : getWebapps()) {
                //theLogger.info("I have a web app of ..... {}", webAppName);
                WebApp w;
                if (webAppName.startsWith(":")) {
                    w = createWebAppConfig(webAppName);
                } else {
                    w = createInstalledAppConfig(webAppName);
                }

                if (w.getWarPath() != null) {
                    wars.add(w.getWarPath());
                }
            }

            List<String> commandLine = new ArrayList<>();

            String javaHome = System.getProperty("java.home");
            String javaBin = javaHome + File.separator + "bin" + File.separator + "java";

            commandLine.add(javaBin);
            if (isDebugEnabled()) {
                commandLine.add("-Xdebug");
                if (isDebugSuspend()) {
                    commandLine.add("-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=" + getDebugPort());
                } else {
                    commandLine.add("-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=" + getDebugPort());
                }
                commandLine.add("-DApplicationDebug=true");
                commandLine.add("-debug");
            }

            commandLine.add("-DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");

            if (getLogLocation() == null) {
                setLogLocation(buildPath + File.separator + "logs");
            }
            commandLine.add("-DApplicationLogs=" + getLogLocation());

            if (getRepoLocation() == null) {
                setRepoLocation(buildPath + File.separator + "repo");
            }
            commandLine.add("-DApplicationRepository=" + getRepoLocation());

            commandLine.add("-DApplicationMode=" + getMode());

            commandLine.add("-DApplicationInstance=" + getInstance());

            // Add any extra VM args
            for (String args : getJvmArgs()) {
                commandLine.add(args);
            }

            //theLogger.quiet("Required classes .."+classpathAsString);
            // Add the classpath and the main class to start
            commandLine.add("-cp");
            commandLine.add(getRequiredJar().getAbsolutePath());
            commandLine.add("gusl.launcher.Launcher");
            if (isLauncherDebug()) {
                commandLine.add("-debug");
            }
            commandLine.add("-location");
            commandLine.add(getProject().getBuildDir().getCanonicalPath());
            commandLine.add("-port");
            commandLine.add(Integer.toString(getPort()));

            if (getHost() != null) {
                commandLine.add("-host");
                commandLine.add(getHost());
            }

            if (getEntitySize() != null) {
                commandLine.add("-entitysize");
                commandLine.add(Long.toString(getEntitySize()));
            }

            if (!wars.isEmpty()) {
                // Last option must be the wars
                commandLine.add("-war");

                for (String warLocation : wars) {
                    commandLine.add(warLocation);
                }
            }

            if (isDebugEnabled()) {
                theLogger.log(LogLevel.INFO, "Command Line {}", commandLine);
            }

            final ProcessBuilder builder = new ProcessBuilder(commandLine);

            if (getSystemEnv() != null) {
                Map<String, String> environment = builder.environment();
                for (String env : getSystemEnv()) {
                    String data[] = env.split("=");
                    if (data.length == 2) {
                        environment.put(data[0], data[1]);
                    }
                }
            }

            final Process process = builder.start();
            final StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream());
            outputGobbler.start();
            final StreamGobbler errGobbler = new StreamGobbler(process.getErrorStream());
            errGobbler.start();
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    process.destroyForcibly();
                }
            });

            theLogger.quiet("Ctrl-C to exit");
            process.waitFor();
        } catch (Throwable t) {
            theLogger.error("Gunder: error for task {} {}", getName(), getStackTrace(t), t);
        }
    }

    private WebApp createWebAppConfig(String webAppProjectName) throws GunderException {

        WebApp config = new WebApp();

        // Lets the web app project
        Project webProject = getProject().getRootProject().findProject(webAppProjectName);

        // Check to see that's a war thingy
        AbstractArchiveTask warTask = GradleUtils.getWarTask(webProject);

        if (warTask == null) {
            throw new GunderException("Webapp [" + webAppProjectName + "] does not have war defined");
        }

        // Lets get the war file
        File warFile = warTask.getArchivePath();
        if (!warFile.exists()) {
            throw new GunderException("Unable to locate war file [" + warFile.getPath() + "]");
        }

        theLogger.info(" Here is the war file .... " + warFile.getAbsolutePath());
        config.setWarPath(warFile.getAbsolutePath());

        return config;
    }

    private WebApp createInstalledAppConfig(String idAndPath) throws GunderException {
        WebApp config = new WebApp();

        Configuration cfg = this.getProject().getConfigurations().getByName("runtime");

        String id = idAndPath;
        String ctx = null;
        if (idAndPath.contains(":")) {
            String parts[] = idAndPath.split(":");
            id = parts[0];
            ctx = parts[1];
        }

        if (ctx == null) {
            ctx = id;
        }

        for (File f : cfg.getFiles()) {
            if (f.getName().contains(id) && f.getName().endsWith(".war")) {
                config.setContextPath("/" + ctx);
                config.setWarPath(f.getPath());
                break;
            }
        }

        return config;
    }

    /**
     * Just a stupid StreamGobbler that will print out all stuff from the
     * "other" process...
     */
    private static class StreamGobbler extends Thread {

        private final InputStream inputStream;

        private StreamGobbler(InputStream is) {
            inputStream = is;
        }

        @Override
        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
}
