package gusl.gunder;

import java.util.Set;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

/**
 * Collection of Gradle Utils.
 *
 * Gradle has to be one of the most difficult systems to work with, even tho
 * there is a API, its basically reverse engineer to find anything out.
 *
 * @author dhudson
 */
public class GradleUtils {

    private GradleUtils() {
    }

    /**
     * Return the war task for the project if defined.
     *
     * @param project
     * @return ArchiveTask or null if not found
     */
    public static AbstractArchiveTask getWarTask(Project project) {
        Set<Task> warTasks = project.getTasksByName("war", false);

        for (Task task : warTasks) {
            return (AbstractArchiveTask) task;
        }

        return null;
    }

    /**
     * Check to see if a project has a plugin.
     *
     * @param project
     * @param name
     * @return true, if the plugin exists
     */
    public static boolean hasPlugin(Project project, String name) {
        return project.getPlugins().hasPlugin(name);
    }

    public static Configuration getConfiguration(Project project, String name) {
        return project.getConfigurations().getByName(name);
    }

    public static Configuration getArchivesConfiguration(Project project) {
        return getConfiguration(project, "archives");
    }

}
