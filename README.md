# Gunder #

Gunder is a Gradle Plugin which enables the running of Undertow.

![Alt text](./src/main/resources/Gunder.jpeg)

There are some excellent plugins out there like [Gretty](https://github.com/akhikhl/gretty), and [Gradle Tomcat Plugin](https://github.com/bmuschko/gradle-tomcat-plugin),
but neither really do the job we require.  The main requirement for Gunder was the ability to run tomcat locally, quickly, with web sockets enabled.

Gunder allows for Casanova to be started in a separate process and have the ability to run many web apps in the same container.

Gunder will not run from the development classpath, so doesn't restart on changes of files, it depends on the [War plugin](http://www.gradle.org/war_plugin.html) to build the war files.

Gusl-Gunder tasks, all depend on the war task.

### How do I get set up? ###

```groovy
buildscript {
    repositories {
        maven {
            url "${nexusHost}/repository/maven-snapshots/"
        }
        maven {
            url "${nexusHost}/repository/maven-releases/"
        }
    }
    dependencies {
        classpath "gusl.gradle.Gunder:Gunder:${GunderVersion}"
    }
}

apply plugin: 'gusl.gradle.Gunder'

task ('router', type: gusl.gradle.Gunder.GunderRunWarTask) {
    port = 9000
    debugPort = 5006
    debugSuspend = true
    // Set the System Property
    jvmArgs = ['-DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector', '-Xmx1024m']  
}

```
In the above example, two tasks will be created, router and routerDebug.  The debug version allows for debugging of the web application.

Many web applications can be run side one container by using the webapp directive.

```groovy
apply plugin: 'gusl.gradle.Gunder'

task ('smallAdmin', type: lm.gradle.Gunder.GunderRunWarTask) {
    excludeSelf = true
    port = 8080
    debugPort = 5006
    debugSuspend = true
    // Set the System Property
    jvmArgs = ['-DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector', '-Xmx1024m']
    webapp ':router'
    webapp ':dataservice'
    webapp ':admin'
}
```

Note the self exclude, as the root project has the war plugin, Gunder will add itself.

Task Property | Description | Default |
 ---  |  ---  |  --- |
port | Undertow http acceptor port.| 8080 |
debugPort | JVM Debug listening port. | 5006 |
suspend | Wait for the debugger to attach. | false |
jvmArgs | Additional JVM arguments to servlet-container process. | [] |
webapps | List of War projects to add. | If the task defined is in a build.gradle which has the plugin war, it will include itself. |
logLocation | Location of where the log files are to reside | buildDir/logs |
repoLocation | Location of where the message bus repo will reside | buildDir/repo |
mode | Operational Mode | DEV |

Use `Ctrl-C` to terminate the task.

### Notes

### Developer Notes ###

#### How it works ####

The gradle plugin looks for the Gunder tasks, and then creates a debug task associated with the task.  It also sets the war dependency on the task.

A separate java process is created, the plugin locates the built war files.

#### How to change the casanova-launcher release  ####

* Edit gradle.properties and change `launcherVersion` to the desired release.
* Edit build.gradle and modify the plugin version.
* `gradlew publish`

### Who do I talk to? ###

Any comments or issues, please contact Darren Hudson
